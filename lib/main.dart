import 'package:flutter/material.dart';

import 'dart:async';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_in_app_messaging/firebase_in_app_messaging.dart';

void main() {
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Time Tracker Application',
//       theme: ThemeData(primarySwatch: Colors.indigo),
//       home: SignInPage(),
//     );
//   }
// }

class MyApp extends StatelessWidget {
  static FirebaseAnalytics analytics = FirebaseAnalytics();
  static FirebaseInAppMessaging fiam = FirebaseInAppMessaging();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: const Text('US Cellular Demo App'),
      ),
      body: Builder(builder: (BuildContext context) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              // AnalyticsEventExample(),
              ProgrammaticTriggersExample(fiam),
            ],
          ),
        );
      }),
    ));
  }
}

class ProgrammaticTriggersExample extends StatelessWidget {
  const ProgrammaticTriggersExample(this.fiam);

  final FirebaseInAppMessaging fiam;

  Future<void> _sendAnalyticsEvent() async {
    await MyApp.analytics
        .logEvent(name: 'uscc_event', parameters: <String, dynamic>{
      'int': 42, // not required?
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          children: <Widget>[
            const Text(
              "Triggers",
              style: TextStyle(
                fontStyle: FontStyle.italic,
                fontSize: 18,
              ),
            ),
            const SizedBox(height: 8),
            // const Text("Manually trigger events programmatically "),
            const SizedBox(height: 8),
            RaisedButton(
              onPressed: () {
                fiam.triggerEvent('programatic_event');

                Scaffold.of(context).showSnackBar(const SnackBar(
                    content: Text("Triggering event: programatic_event")));
              },
              color: Colors.blue,
              child: Text(
                "Programmatic Triggers".toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            ),
            RaisedButton(
              onPressed: () {
                _sendAnalyticsEvent();
                Scaffold.of(context).showSnackBar(const SnackBar(
                    content:
                        Text("Firing analytics event: uscc_analytics_event")));
              },
              color: Colors.blue,
              child: Text(
                "Analytics event".toUpperCase(),
                style: TextStyle(color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
